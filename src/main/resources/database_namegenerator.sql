-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for nimeandmebaas
DROP DATABASE IF EXISTS `nimeandmebaas`;
CREATE DATABASE IF NOT EXISTS `nimeandmebaas` /*!40100 DEFAULT CHARACTER SET latin7 COLLATE latin7_estonian_cs */;
USE `nimeandmebaas`;

-- Dumping structure for table nimeandmebaas.nimed
DROP TABLE IF EXISTS `nimed`;
CREATE TABLE IF NOT EXISTS `nimed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nimi` varchar(150) COLLATE latin7_estonian_cs NOT NULL,
  `sugu` char(1) COLLATE latin7_estonian_cs NOT NULL,
  `kas_rahvusvaheline` char(1) COLLATE latin7_estonian_cs NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1632 DEFAULT CHARSET=latin7 COLLATE=latin7_estonian_cs;

-- Data exporting was unselected.
-- Dumping structure for table nimeandmebaas.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) DEFAULT NULL,
  `username` varchar(200) COLLATE latin7_estonian_cs DEFAULT NULL,
  `password` varchar(200) COLLATE latin7_estonian_cs DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin7 COLLATE=latin7_estonian_cs;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
