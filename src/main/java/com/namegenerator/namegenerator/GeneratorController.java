package com.namegenerator.namegenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@RestController

public class GeneratorController {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/greeting")
    public String getGreeting() {
        return "Tere!!!";
    }

    @PostMapping("/names/upload")
    public void uploadNames(@RequestParam MultipartFile namesFile) throws IOException {
        Files.copy(namesFile.getInputStream(), Paths.get("C:/uploads/uploadednames.csv"), StandardCopyOption.REPLACE_EXISTING);

        // Lugeda sisse fail rida haaval (vihje Files.readAllLines())

        // Käia for-tsükliga list läbi

        // printinda iga rida standardväljundisse

        // sisestame selle rea nimena andmebaasi
//

        try {
            List<String> lines = Files.readAllLines(Paths.get("C:/uploads/uploadednames.csv"), StandardCharsets.UTF_8);
            for (String s : lines) {
                String[] oneLine = s.split(",");
                // insert into
                jdbcTemplate.update("insert into nimed (nimi, sugu, kas_rahvusvaheline) values (?, ?, ?)",
                        oneLine[0], oneLine[1], oneLine[2]);
            }
        } catch (IOException ex) {
            System.out.println("Faili ei saa avada.");
        }
    }
//    @GetMapping("/names")
//    public String getNames() {
//        return names();
//    }

    @GetMapping("/admin/names")
    public List<Name> getNames() {
        List<Name> names = jdbcTemplate.query("select * from nimed order by nimi asc", (row, count) -> {
                    int id = row.getInt("id");
                    String babyName = row.getString("nimi");
                    String babySex = row.getString ("sugu");
                    String babyInternational = row.getString("kas_rahvusvaheline");

                    Name name = new Name();
                    name.setId(id);
                    name.setName(babyName);
                    name.setSex(babySex);
                    name.setInternational(babyInternational);

                    return name;
                }
        );

        return names;
    }

    // tekitada uus POST-meetod nimega näiteks "/search"
    // luua klass SearchFilter, milles oleksid olemas otsinguväljad
    //  - poiss, tüdruk, ...
    // kirjutada SQL päring

    @PostMapping("/admin/name")
    public void addName(@RequestBody Name name) {
        jdbcTemplate.update("insert into nimed (nimi, sugu, kas_rahvusvaheline) values (?, ?, ?)", name.getName(), name.getSex(), name.getInternational());
    }

    @PostMapping("/names/search")
    public List<Name> searchNames(@RequestBody SearchFilter filter) {
        String where = " where 1=1 ";
        if (filter.isBoySelected()) {
            where += " and sugu in ('P', 'U')";
        } else if (filter.isUnisexSelected()) {
            where += " and sugu in ('U')";
        } else if (filter.isGirlSelected()) {
            where += " and sugu in ('T', 'U')";
        }

        if (filter.isInternationalSelected()) {
            where += " and kas_rahvusvaheline in ('R')";
        }

        if (filter.getLengthSelection()>0) {
            where += " and length(nimi) = " + filter.getLengthSelection();
        }

        if (filter.getFirstLetter()>0) {
            where += " and lower(nimi) like " + "lower('" + filter.getFirstLetter() + "%')";
        }

        if (filter.getOneLetter()>0) {
            where += " and lower(nimi) like " + "lower('%" + filter.getOneLetter() + "%')";
        }

        if (filter.getLastLetter()>0) {
            where += " and lower(nimi) like " + "lower('%" + filter.getLastLetter() + "')";
        }

        List<Name> searchNames = jdbcTemplate.query("select * from nimed " + where + " order by nimi asc", (row, count) -> {
                    int id = row.getInt("id");
                    String babyName = row.getString("nimi");
                    Name name = new Name();
                    name.setId(id);
                    name.setName(babyName);
                    return name;
                }
        );

        return searchNames;
    }

    @DeleteMapping("/admin/name/{id}")
    public void deleteName(@PathVariable int id) {
        jdbcTemplate.update("delete from nimed where id = ?", id);
    }
}