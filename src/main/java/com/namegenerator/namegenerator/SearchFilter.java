package com.namegenerator.namegenerator;

public class SearchFilter {
    public boolean isBoySelected() {
        return boySelected;
    }

    public void setBoySelected(boolean boySelected) {
        this.boySelected = boySelected;
    }

    public boolean isUnisexSelected() {
        return unisexSelected;
    }

    public void setUnisexSelected(boolean unisexSelected) {
        this.unisexSelected = unisexSelected;
    }

    public boolean isGirlSelected() {
        return girlSelected;
    }

    public void setGirlSelected(boolean girlSelected) {
        this.girlSelected = girlSelected;
    }

    public boolean isInternationalSelected() {
        return internationalSelected;
    }

    public void setInternationalSelected(boolean internationalSelected) {
        this.internationalSelected = internationalSelected;
    }

    private boolean boySelected;
    private boolean unisexSelected;
    private boolean girlSelected;
    private boolean internationalSelected;

    public int getLengthSelection() {
        return lengthSelection;
    }

    public void setLengthSelection(int lengthSelection) {
        this.lengthSelection = lengthSelection;
    }

    public char getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(char firstLetter) {
        this.firstLetter = firstLetter;
    }

    public char getOneLetter() {
        return oneLetter;
    }

    public void setOneLetter(char oneLetter) {
        this.oneLetter = oneLetter;
    }

    public char getLastLetter() {
        return lastLetter;
    }

    public void setLastLetter(char lastLetter) {
        this.lastLetter = lastLetter;
    }

    private int lengthSelection;
    private char firstLetter;
    private char oneLetter;
    private char lastLetter;
}
