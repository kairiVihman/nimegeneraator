package com.namegenerator.namegenerator;

//public class Name {
//    private int id;
//    private String nimi;
//    private char sugu;
//    private char kas_rahvusvaheline;
//
//    public String getNimi() {
//        return nimi;
//    }
//
//    public void setNimi(String nimi) {
//        this.nimi = nimi;
//    }
//
//    public char getSugu() {
//        return sugu;
//    }
//
//    public void setSugu(char sugu) {
//        this.sugu = sugu;
//    }
//
//    public char getKas_rahvusvaheline() {
//        return kas_rahvusvaheline;
//    }
//
//    public void setKas_rahvusvaheline(char kas_rahvusvaheline) {
//        this.kas_rahvusvaheline = kas_rahvusvaheline;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return nimi;
//    }
//
//    public void setName(String name) {
//        this.nimi = name;
//    }
//
//    public char getSex() {
//        return sugu;
//    }
//
//    public void setSex(char sex) {
//        this.sugu = sex;
//    }
//
//    public char getInternational() {
//        return kas_rahvusvaheline;
//    }
//
//    public void setInternational(char international) {
//        this.kas_rahvusvaheline = international;
//    }
//
//}

public class Name {
    private int id;
    private String name;
    private String sex;
    private String international;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }
}
